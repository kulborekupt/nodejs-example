const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;


const smartGate = require('./smartgates/user_permission');
// Define a route
app.get('/api', (req, res) => {
    var userId = req.query.user_id;
    var gateId = req.query.gate_id;

    let permission = smartGate.validUserPermission(userId, gateId,res)
       
});

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});