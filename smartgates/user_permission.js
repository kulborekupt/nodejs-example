

const objectConnection = require('../connection');

function validUserPermission(userId, gateId,res) {

    userId = objectConnection.mysql.escape(userId);
    gateId = objectConnection.mysql.escape(gateId);

    let query = "SELECT * FROM sg_view_rights WHERE user_id = " + userId + "  AND gate_id = " + gateId;
    var user_permission = {};
    var response = false;
    objectConnection.connection.query(query, (err, results, fields) => {
        if (err) {
            console.error('Error executing query:', err);
            // return false;
            res.json({"Permission":"fail"})
        } else {
            user_permission = results;
            console.log(results)
            // return true;
            response = true
            res.json({"Permission":"success"})
        }
    });
}

module.exports = {
    validUserPermission,
};